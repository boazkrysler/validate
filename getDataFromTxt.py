import subprocess
import logging

def removeDuplicate(errorSaver, lines):
    returnList = []
    dontAddList = [0] * len(errorSaver)

    for i in range(0, len(errorSaver)):
        tag = errorSaver[i][2]
        tag2 = errorSaver[i][3]
        line = errorSaver[i][1]

        for y in range(0, i):
            if tag == errorSaver[y][2]:
                if tag2 == errorSaver[y][3] and line[0] == (errorSaver[y][1])[0]:

                    if line[0] == (errorSaver[y][1])[0]:
                        errorStartFrame, errorEndrtFrame = findNumberOfErrors(line[5], tag, tag2, lines, line[0])
                        errorStartFrame, errorEndrtFrame2 = findNumberOfErrors((errorSaver[y][1])[5], errorSaver[y][2],
                                                                               errorSaver[y][3], lines,
                                                                               (errorSaver[y][1])[0])

                        if errorEndrtFrame == errorEndrtFrame2:
                            dontAddList[y] = 1

    for i in range(0, len(errorSaver)):
        if dontAddList[i] == 0:
            returnList.append(errorSaver[i])

    return returnList


def findNumberOfErrors(startingTime, tag, tag2, lines, id):
    endFrame = 0

    for i in range(0, len(lines)):
        line = lines[i].split(' ')
        if line[0] == id:
            if line[5] == startingTime:

                nextSegment_FrameNum = 321
                while nextSegment_FrameNum < int(startingTime):
                    nextSegment_FrameNum += 300

                for y in range(i, len(lines) - 1):
                    line2 = lines[y].split(' ')
                    found = 0
                    endFrame = line2[5]

                    if line2[0] != id:
                        line2 = lines[y - 1].split(' ')
                        return int(startingTime), int(line2[5])

                    for h in range(10, len(line2)):
                        if line2[h].translate(None, '"') == tag or line2[h].translate(None, '"') == tag2:
                            found += 1
                    if found < 2:
                        return int(startingTime), int(endFrame)

                    if int(line2[5]) == nextSegment_FrameNum:
                        return int(startingTime), nextSegment_FrameNum

    return int(startingTime), int(endFrame)


def findVideoLink(videoName):
    cmdLine = "cd /root/vatic ; ./list_assignments.sh " + videoName + " | awk '{ print $10 }' "
    p = subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
    stdout, stderr = p.communicate(b"input data that is passed to subprocess' stdin")

    videoLink = stdout.split('\n')
    logging.debug("findVideoLink: cmdLine: %s", cmdLine)

    return videoLink[2:-1]

def findVideoLength(videoName):
    cmdLine = "cd /root/vatic ; ./list_segments.sh " + videoName + " | awk '{ print $3 }' "
    p = subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
    stdout, stderr = p.communicate(b"input data that is passed to subprocess' stdin")
    videoLength = stdout.split('\n')
    return videoLength


def objectExitFrame(id, lines):
    for i in range(0, len(lines)):
        line = lines[i].split(' ')
        if line[0] == id:
            if line[6] == "0":
                for y in range(i, len(lines) - 1):
                    line = lines[y].split(' ')
                    if line[6] == "1":
                        return line[5]
                return "never"

    return "never"


def entereFrame(id, lines):
    for i in range(0, len(lines)):
        line = lines[i].split(' ')
        if line[0] == id:
            if line[6] == "0":
                return line
    # error:
    line = lines[0].split(' ')
    line[1] = -1
    return line


def findNumOfFrames(lines):
    max = 0
    for i in range(0, len(lines)-1):
        line = lines[i].split(' ')
        if int(line[5]) > max:
            max = int(line[5])

    return max