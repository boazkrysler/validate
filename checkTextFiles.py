import csv
import errno
import glob
import os
import logging
import subprocess
import datetime

from getDataFromTxt import removeDuplicate, findNumberOfErrors, findVideoLink, objectExitFrame, entereFrame, \
    findVideoLength, findNumOfFrames
from imageHandle import addErrorBB
from imageHandle import copyImageFromStorage, create_video_images_folder_if_dont_exist
from runSHH import createReportFolder
from sqlVideosFromServer import findVideosSizeByName
import ConfigParser

config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))
path_save_reports_and_images = config.get('paths', 'path_save_reports_and_images')
path_save_turkic_files = config.get('paths', 'path_save_turkic_files')
path_csv_files = config.get('paths', 'path_csv_files')

global errorCounter, FRAMES_WITH_ERRORS_COUNTER
errorCounter = 0
FRAMES_WITH_ERRORS_COUNTER = 0

def printReportToFile(lines):
    global errorSaver, videoLink, fileName, errorCounter, date, FRAMES_WITH_ERRORS_COUNTER, DATE_OF_EXEC
    FRAMES_WITH_ERRORS_COUNTER = 0

    create_video_images_folder_if_dont_exist(fileName)

    errorSaver = removeDuplicate(errorSaver, lines)
    width, height = findVideosSizeByName(fileName)
    segmentsLength = findVideoLength(fileName)

    width = int(width)
    height = int(height)
    imageFound = True
    for i in range(0, len(errorSaver)):

        errorCounter += 1

        tag = errorSaver[i][2]
        tag2 = errorSaver[i][3]
        thisLine = errorSaver[i][1]

        logging.debug("printReportToFile: tag1: %(s1)s ,tag2: %(s2)s ,frame: %(s3)s ,imageFound: %(s4)s", {'s1':tag,'s2':tag2,'s3':thisLine[5], 's4': str(imageFound)})

        ErrorBBX = str(int((float(thisLine[1]) / width) * 100)) + "%"
        ErrorBBY = str(int((float(thisLine[2]) / height) * 100)) + "%"

        errorStartFrame, errorEndFrame = findNumberOfErrors(thisLine[5], tag, tag2, lines, thisLine[0])
        frameErrorLength = errorEndFrame - errorStartFrame
        FRAMES_WITH_ERRORS_COUNTER += frameErrorLength

        frameNum = int(thisLine[5])
        segmentNum = 1
        while frameNum > 321:
            frameNum -= 300
            segmentNum += 1

        videoLinkForSegment = videoLink[segmentNum - 1]
        segLength = int(segmentsLength[segmentNum - 1]) - 300 * (segmentNum - 1)

        otherAttributes = str(thisLine[10:])
        objectEnterFrameLine = entereFrame(thisLine[0], lines)

        pathEnterBBX = str(int((float(objectEnterFrameLine[1]) / width) * 100)) + "%"
        pathEnterBBY = str(int((float(objectEnterFrameLine[2]) / height) * 100)) + "%"


        ##Image handling:
        imageFound = copyImageFromStorage(fileName, frameNum=thisLine[5], errorCounter=errorCounter, date=date,
                                              x1=thisLine[1], x2=thisLine[3], y1=thisLine[2], y2=thisLine[4],
                                              mergeRate=MERGE_RATE, date_of_exec=DATE_OF_EXEC)

        if not imageFound:
            pathToImage = "Missing"
        else:
            pathToImage = "http://169.55.111.170:8080" + path_save_reports_and_images + "/" + DATE_OF_EXEC + "/merge_rate_" + MERGE_RATE + "/" + date + "/" + str(
                errorCounter) + ".jpg"

        visible = True
        if thisLine[6] == "0":
            visible = False
        csvWriter.writerow(
            {'Segment URL': videoLinkForSegment, 'Video Name': fileName, 'Image URL': pathToImage, 'tag1': tag,
             'tag2': tag2, 'visibale?': visible, 'Track Attributes': otherAttributes,
             'Error Nr': errorCounter, 'Error in Segment Nr': segmentNum, 'Track ID': thisLine[0],
             'Total Affected Frames': frameErrorLength, 'Segment Length': segLength,
             'Error Start Frame': frameNum, 'Error End Frame': errorEndFrame,
             'Error BB - top left  X': ErrorBBX, 'Error BB - top left Y': ErrorBBY,
             'Track ID Start in Segment': int(objectEnterFrameLine[5]) / 321 + 1,
             'Track Start Frame Nr': int(objectEnterFrameLine[5]) % 321,
             'Track End Frame Nr': objectExitFrame(thisLine[0], lines), 'Track Start BB - X': pathEnterBBX,
             'Track Start BB - Y': pathEnterBBY,
             })


def saveError(videoLink, line, tag, tag2):
    global errorSaver
    lst = (videoLink, line, tag, tag2)
    errorSaver.append(lst)


def sendError(line, tag, tag2):
    global videoLink
    saveError(videoLink, line, tag, tag2)


def findSimilerAttributes(line, tags, data):
    global lastErrorFound

    frameNum = int(line[5])
    while frameNum > 321:  # last frame in this segment
        frameNum -= 300

    first = True
    for i in range(0, len(tags)):
        for y in range(0, len(data)):
            if tags[i].translate(None, '"') == data[y]:
                if first:
                    tag = data[y]
                    first = False
                else:
                    if lastErrorFound != tags or frameNum == 22:
                        lastErrorFound = tags
                        sendError(line, tag, data[y])


def person():
    global line

    # person taggings:
    personUpperData = ["BLACK-UPPER", "BLUE-UPPER", "BROWN-UPPER", "GREEN-UPPER", "GREY-UPPER", "PINK-UPPER",
                       "PURPLE-UPPER", "ORANGE-UPPER", "RED-UPPER", "WHITE-UPPER", "YELLOW-UPPER", "NO-UPPER"]
    personLowerData = ["BLACK-LOWER", "BLUE-LOWER", "BROWN-LOWER", "GREEN-LOWER", "GREY-LOWER", "PINK-LOWER",
                       "PURPLE-LOWER", "ORANGE-LOWER", "RED-LOWER", "WHITE-LOWER", "YELLOW-LOWER", "NO-LOWER"]
    personMovingData = ["PERSON_RUNNING", "PERSON_WALKING", "RIDING_BICYCLE", "RIDING_MOTORCYCLE", "RIDING_ANIMAL",
                        "RIDING_SCOOTER", "PERSON_STANDING", "PERSON_CROUCHING", "PERSON_LAYING_ON_FLOOR",
                        "PERSON_SEATING", "PERSON_JUMPING", "PERSON_CLIMBING", "PERSON_MOVING"]
    personTypeData = ["SOLDIER", "POLICEMAN", "UNIFORMED"]
    personGenderData = ["MALE", "FEMALE"]
    personAgeData = ["ADULT", "TEENAGER", "CHILD"]

    tags = line[10:]

    findSimilerAttributes(line, tags, personUpperData)
    findSimilerAttributes(line, tags, personLowerData)
    findSimilerAttributes(line, tags, personMovingData)
    findSimilerAttributes(line, tags, personTypeData)
    findSimilerAttributes(line, tags, personGenderData)
    findSimilerAttributes(line, tags, personAgeData)


def car():
    carTypeData = ["TRUCK", "BUS", "VAN", "PICKUP_TRUCK", "PRIVATE", "JEEP", "CONVERTIBLE", "TRACTOR", "MILITARY_JEEP",
                   "AMBULANCE", "TRAILER", "POLICE_CAR"]
    colorData = ["BLACK", "GREY", "WHITE", "RED", "ORANGE", "YELLOW", "BROWN", "PURPLE", "GREEN", "BLUE", "PINK"]

    global line
    tags = line[10:]

    findSimilerAttributes(line, tags, carTypeData)
    findSimilerAttributes(line, tags, colorData)


def motorcycle():
    colorData = ["BLACK", "GREY", "WHITE", "RED", "ORANGE", "YELLOW", "BROWN", "PURPLE", "GREEN", "BLUE", "PINK"]

    global line
    tags = line[10:]

    findSimilerAttributes(line, tags, colorData)


def train():
    colorData = ["BLACK", "GREY", "WHITE", "RED", "ORANGE", "YELLOW", "BROWN", "PURPLE", "GREEN", "BLUE",
                 "PINK"]
    typeData = ["TRAIN", "TRAM"]
    global line
    tags = line[10:]

    findSimilerAttributes(line, tags, colorData)
    findSimilerAttributes(line, tags, typeData)


def objectCase():
    colorData = ["BLACK", "GREY", "WHITE", "RED", "ORANGE", "YELLOW", "BROWN", "PURPLE", "GREEN", "BLUE",
                 "PINK"]
    typeData = ["BACKPACK", "SUITCASE", "HANDBAG"]
    weaponData = ["GUN", "RIFLE", "KNIFE", "BAT"]
    global line
    tags = line[10:]

    findSimilerAttributes(line, tags, colorData)
    findSimilerAttributes(line, tags, typeData)
    findSimilerAttributes(line, tags, weaponData)


def crowd():
    typeData = ["CROWD_MOVING", "CROWD_RUNNING"]

    global line
    tags = line[10:]

    findSimilerAttributes(line, tags, typeData)


def onlyOneType():
    pass


# map the inputs to the function blocks
tagOptions = {'"PERSON"': person,
              '"CAR"': car,
              '"MOTORCYCLE"': motorcycle,
              '"TRAIN"': train,
              '"OBJECT"': objectCase,
              '"CROWD"': crowd,
              '"POLE"': onlyOneType,
              '"TREE"': onlyOneType,
              '"PEOPLE_RUNNING_ONE_AFTER_THE_OTHER"': onlyOneType,
              '"PERSON_GET_INTO_CAR"': onlyOneType,
              '"PERSON_GET_OUT_FROM_CAR"': onlyOneType,
              '"BUS_STOP"': onlyOneType,
              '"ATM"': onlyOneType,
              '"TRAILER"': onlyOneType,
              '"BICYCLE"': onlyOneType,
              }


def checkAttributes(thisLine):
    global line
    line = thisLine.split(' ')
    if len(line) > 9:
        try:
            tagOptions[line[9]]()
        except:
            pass

def create_csv_files(mergeRate, part):
    global DATE_OF_EXEC
    file_name = "/videos_with_errors_up_to_date_" + DATE_OF_EXEC + "_merge_rate_" + mergeRate + "_" + part + ".csv"
    my_file = path_csv_files + file_name
    if not os.path.isfile(my_file):
        with open(my_file, 'w') as csvfile:
            fieldnames = ['Video_Name', 'Nb of segments', 'Nb of frames in video', 'Nb of errors',
                          'Sum of errors length in frames', 'Video date', 'link to report']

            csvWriter = csv.DictWriter(csvfile, fieldnames=fieldnames)
            csvWriter.writeheader()

            cmdLine = "sudo chmod -R 777 " + my_file
            subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)


    file_name = "/videos_with_no_errors_up_to_date_" + DATE_OF_EXEC + "_merge_rate_" + mergeRate + "_" + part + ".csv"
    my_file = path_csv_files + file_name
    if not os.path.isfile(my_file):
        with open(my_file, 'w') as csvfile:
            fieldnames = ['Video_Name', 'Nb of segments', 'Nb of frames in video', 'Video date', 'link to report']

            csvWriter = csv.DictWriter(csvfile, fieldnames=fieldnames)
            csvWriter.writeheader()
            cmdLine = "sudo chmod -R 777 " + my_file
            subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)

def printNoErrorsToCsv(lines, part):
    global fileName, date, DATE_OF_EXEC
    logging.info("no errors found in video: %s", fileName)
    frames = findNumOfFrames(lines)
    segmentNum = 1
    frameNum = frames
    while frameNum > 321:
        frameNum -= 300
        segmentNum += 1

    csvFileName = "videos_with_no_errors_up_to_date_" + DATE_OF_EXEC + "_merge_rate_" + MERGE_RATE + "_" + part + ".csv"
    my_file = path_csv_files + "/" + csvFileName

    with open(my_file) as infile:
        r = csv.DictReader(infile, skipinitialspace=True)
        for row in r:
            for val in row.itervalues():
                if val == fileName:
                    return

    link_to_report = "http://169.55.111.170:8080" + path_save_reports_and_images + "/" + DATE_OF_EXEC + "/merge_rate_" + MERGE_RATE + "/" + date + "/" \
                     + "Report_" + date + "_merge_rate_" + MERGE_RATE + ".csv"
    with open(my_file, 'a') as csvfile:
        fieldnames = ['Video_Name', 'Nb of segments', 'Nb of frames in video', 'Video date','link to report']

        csvWriter = csv.DictWriter(csvfile, fieldnames=fieldnames)
        csvWriter.writerow(
            {'Video_Name': fileName, 'Nb of segments': segmentNum, 'Nb of frames in video': frames, 'Video date': date,
             'link to report': link_to_report})


def printNumOfErrorsToCsv(errorSaver, lines, part):
    global fileName, date, DATE_OF_EXEC
    frames = findNumOfFrames(lines)
    segmentNum = 1
    frameNum = frames
    while frameNum > 321:
        frameNum -= 300
        segmentNum += 1

    csvFileName = "videos_with_errors_up_to_date_" + DATE_OF_EXEC + "_merge_rate_" + MERGE_RATE + "_" + part + ".csv"
    my_file = path_csv_files + "/" + csvFileName
    with open(my_file) as infile:
        r = csv.DictReader(infile, skipinitialspace=True)
        for row in r:
            for val in row.itervalues():
                if val == fileName:
                    return

    link_to_report = "http://169.55.111.170:8080" + path_save_reports_and_images + "/" + DATE_OF_EXEC + "/merge_rate_" + MERGE_RATE + "/" + date + "/" \
                     + "Report_" + date + "_merge_rate_" + MERGE_RATE + ".csv"
    with open(my_file, 'a') as csvfile:
        fieldnames = ['Video_Name', 'Nb of segments', 'Nb of frames in video', 'Nb of errors',
                      'Sum of errors length in frames', 'Video date', 'link to report']

        csvWriter = csv.DictWriter(csvfile, fieldnames=fieldnames)

        csvWriter.writerow(
            {'Video_Name': fileName, 'Nb of segments': segmentNum, 'Nb of frames in video': frames,
             'Nb of errors': len(errorSaver),
             'Sum of errors length in frames': FRAMES_WITH_ERRORS_COUNTER, 'Video date': date, 'link to report': link_to_report})


def readFile(txtFile, part):
    global errorSaver, NUM_OF_VIDEOS_WITH_ERRORS
    lines = txtFile.read().split('\n')
    errorSaver = []

    for i in range(0, len(lines)):
        checkAttributes(lines[i])

    segmentsLength = findVideoLength(fileName)

    logging.debug("Video " + fileName[0:10] + " have " + str(segmentsLength[-2] + " frames"))

    if len(errorSaver) > 0:
        printReportToFile(lines)
        printNumOfErrorsToCsv(errorSaver, lines, part)
        NUM_OF_VIDEOS_WITH_ERRORS += 1
    else:
        printNoErrorsToCsv(lines, part)

global line
line = ""


def copyCsvToReportFolder(csvFileName, part):
    global date,MERGE_RATE, DATE_OF_EXEC
    cwd = path_csv_files

    cmdLine = "sudo mv " + os.getcwd() + "/" + csvFileName + " " + path_save_reports_and_images + "/" + DATE_OF_EXEC + "/merge_rate_" + MERGE_RATE + "/" + date
    p = subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
    p.communicate(b"input data that is passed to subprocess' stdin")

    cmdLine = "sudo cp " + cwd + "/videos_with_errors_up_to_date_" + DATE_OF_EXEC + "_merge_rate_" + MERGE_RATE + "_" + part + ".csv " + path_save_reports_and_images + "/" + DATE_OF_EXEC
    p = subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
    p.communicate(b"input data that is passed to subprocess' stdin")

    cmdLine = "sudo cp " + cwd + "/videos_with_no_errors_up_to_date_" + DATE_OF_EXEC + "_merge_rate_" + MERGE_RATE + "_" + part + ".csv " + path_save_reports_and_images + "/" + DATE_OF_EXEC
    p = subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
    p.communicate(b"input data that is passed to subprocess' stdin")


def getVideoName(name):
    list = name.split('/')
    name = list[-1]
    return name[:-4]

def runOnAllTextsFilesInFolder(path, todayDate, mergeRate, part, date_of_exec):
    global videoLink, csvWriter, fileName, date, MERGE_RATE, DATE_OF_EXEC
    DATE_OF_EXEC = date_of_exec
    MERGE_RATE = mergeRate
    date = todayDate
    csvFileName = "Report_" + date + "_merge_rate_" + MERGE_RATE + ".csv"

    createReportFolder(date, mergeRate, DATE_OF_EXEC)
    create_csv_files(mergeRate, part)


    with open(csvFileName, 'w') as csvfile:
        fieldnames = ['Segment URL', 'Video Name', 'Image URL', 'tag1', 'tag2' ,'visibale?', 'Track Attributes',
                      'Error Nr', 'Error in Segment Nr', 'Track ID',
                      'Total Affected Frames', 'Segment Length',
                      'Error Start Frame', 'Error End Frame', 'Error BB - top left  X', 'Error BB - top left Y',
                      'Track ID Start in Segment', 'Track Start Frame Nr', 'Track End Frame Nr', 'Track Start BB - X',
                      'Track Start BB - Y']

        csvWriter = csv.DictWriter(csvfile, fieldnames=fieldnames)
        csvWriter.writeheader()
        totalFilesRead = 0

        files = glob.glob(path + "*.txt")
        for name in files:  # 'file' is a builtin type, 'name' is a less-ambiguous variable name.
            try:
                with open(name) as f:  # No need to specify 'r': this is the default.
                    fileName = getVideoName(name)
                    logging.info("opening txt file for reading: %s", str(fileName))
                    videoLink = findVideoLink(fileName)
                    readFile(f, part)
                    totalFilesRead += 1
            except IOError as exc:
                if exc.errno != errno.EISDIR:  # Do not fail if a directory is found, just ignore it.
                    raise  # Propagate other kinds of IOError.

    logging.info("found " + str(FRAMES_WITH_ERRORS_COUNTER) + " errors in " + str(NUM_OF_VIDEOS_WITH_ERRORS) + " videos from total of " + str(totalFilesRead) + " videos")
    copyCsvToReportFolder(csvFileName, part)


global lastErrorFound, videoLink, wb, ws, errorSaver, fileName, date, csvWriter,NUM_OF_VIDEOS_WITH_ERRORS, MERGE_RATE , DATE_OF_EXEC
lastErrorFound = ""
errorSaver = []
NUM_OF_VIDEOS_WITH_ERRORS = 0
