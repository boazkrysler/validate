import subprocess
import logging

import os
import ConfigParser

config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))
logging_level = config.get('logging', 'logging_level')
path_save_turkic_files = config.get('paths', 'path_save_turkic_files')
path_save_reports_and_images = config.get('paths', 'path_save_reports_and_images')
path_save_temp_pic = config.get('paths', 'path_save_temp_pic')
path_images_on_storage = config.get('paths', 'path_images_on_storage')
path_csv_files = config.get('paths', 'path_csv_files')

def turkicDumpCommand(videoName, date, mergeRate, path):



    cmdLine = "cd /root/vatic ; sudo turkic dump " + videoName + " -o " + path + "/" + videoName + ".txt --merge --merge-threshold " + mergeRate

    if mergeRate == "0":
        cmdLine = "cd /root/vatic ; sudo turkic dump " + videoName +  " -o " + path +  "/" + videoName + ".txt"
    p = subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
    p.communicate(b"input data that is passed to subprocess' stdin")

    logging.debug("turkicDumpCommand: cmdLine: %s", cmdLine)

def turkicVideos(videoNames, date, path_turkic_txt_files_with_merge, mergeRate):
    # createFolderForFiles(ssh,date)
    createFolder(path_save_turkic_files)
    createFolder(path_turkic_txt_files_with_merge)
    createFolder(path_turkic_txt_files_with_merge + "/" + date)

    video_name = ""
    for i in range(0, len(videoNames)):

        if video_name != videoNames[i]:
            turkicDumpCommand(videoNames[i], date, mergeRate, path_turkic_txt_files_with_merge + "/" + date)
            video_name = videoNames[i]

def createFolder(path):
    if not os.path.exists(path):
        subprocess.call(['sudo', 'mkdir', path])


def createReportFolder(date, mergeRate, DATE_OF_EXEC):
    cwd = os.getcwd()

    createFolder("/tmp")
    createFolder(path_csv_files)

    createFolder(cwd + "/csv_files")
    createFolder(path_save_reports_and_images)
    createFolder(path_save_reports_and_images + "/" + DATE_OF_EXEC)
    createFolder(path_save_reports_and_images + "/" + DATE_OF_EXEC + "/merge_rate_" + mergeRate)
    createFolder(path_save_reports_and_images + "/" + DATE_OF_EXEC + "/merge_rate_" + mergeRate + "/" + date)
    createFolder(path_save_temp_pic)
    createFolder(path_save_temp_pic + "/merge_rate_" + mergeRate)
    createFolder(path_save_temp_pic + "/merge_rate_" + mergeRate + "/" + date)
    createFolder(path_save_temp_pic + "/withErrorBB_merge_rate_" + mergeRate)
    createFolder(path_save_temp_pic + "/withErrorBB_merge_rate_" + mergeRate + "/" + date)

    #give chmod 777
    cmdLine = "sudo chmod -R 777 " + path_save_temp_pic + "/"
    subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)

    cmdLine = "sudo chmod -R 777 " + path_csv_files + "/"
    subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)

    cmdLine = "sudo chmod -R 777 " + path_save_turkic_files + "/"
    subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)

    cmdLine = "sudo chmod -R 777 " + cwd + "/csv_files/"
    subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)

    cmdLine = "sudo chmod -R 777 " + cwd
    subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)