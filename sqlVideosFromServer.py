import MySQLdb
import logging

def findVideosNameByDate(date):
    command = "select slug from videos v join segments s on v.id=s.videoid join jobs j  on j.segmentid=s.id join" \
              " turkic_hits t on j.id=t.id where cast(t.timecompleted as DATE)= '" + date + "'"

    db = MySQLdb.connect("localhost", "root", "viisights5344", "vatic")
    db.query(command)

    data = db.store_result()
    data = data.fetch_row(0)

    videoNamesList = list(data)
    for i in range(0, len(videoNamesList)):
        videoNamesList[i] = videoNamesList[i][0]

    db.close()

    logging.debug("findVideosNameByDate: sql command: %s", command)
    return videoNamesList

def find_video_location(video_name):
    command = "select file_name from videos where slug = '" + video_name + "'"

    db = MySQLdb.connect("localhost", "root", "viisights5344", "vatic")
    db.query(command)

    data = db.store_result()
    data = data.fetch_row(0)

    videoNamesList = list(data)
    for i in range(0, len(videoNamesList)):
        videoNamesList[i] = videoNamesList[i][0]

    db.close()

    logging.debug("find_video_location: sql command: %s", command)
    logging.debug("video name return: " +str(videoNamesList[0]))
    return videoNamesList[0]

def findVideosSizeByName(videoName):
    command = "select width, height from videos v where slug = '" + videoName + "'"

    db = MySQLdb.connect("localhost", "root", "viisights5344", "vatic")
    db.query(command)

    data = db.store_result()
    data = data.fetch_row()

    videoNamesList = list(data)
    logging.debug("findVideosSizeByName: sql command: %s", command)
    db.close()

    return videoNamesList[0][0], videoNamesList[0][1]
