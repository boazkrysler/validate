import MySQLdb
from datetime import datetime, timedelta
import sys
import subprocess
import logging
import ConfigParser

config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))
logging_level = config.get('logging', 'logging_level')
path_save_turkic_files = config.get('paths', 'path_save_turkic_files')
path_save_temp_pic = config.get('paths', 'path_save_temp_pic')


def findVideosDates(date):

    command = "select date(t.timecompleted) from videos v join segments s on v.id=s.videoid join jobs j  on j.segmentid=s.id" \
              " join turkic_hits t on j.id=t.id where cast(t.timecompleted as DATE) < '" + date + "' group by date(timecompleted)"
    db = MySQLdb.connect("localhost", "root", "viisights5344", "vatic")
    db.query(command)

    data = db.store_result()
    data = data.fetch_row(0)

    videoDatesList = list(data)
    for i in range(0, len(videoDatesList)):
        videoDatesList[i] = videoDatesList[i][0]

    db.close()
    logging.debug("findVideosDates: cmdLine: %s", command)

    return videoDatesList

def runValidateOnVideos(date, merge_rate,part, logname, date_of_exec):
    cmdLine = "python validate_daily_tagging.py " + date + " " + merge_rate + " part_" + str(part) + " " + logname + " " + date_of_exec
    p = subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
    p.communicate(b"input data that is passed to subprocess' stdin")
    logging.debug("findVideosDates: cmdLine: %s", cmdLine)

def delete_temp_files():
    try:
        cmdLine = "sudo rm -rf " + path_save_temp_pic + "/*"
        subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)

        cmdLine = "sudo rm -rf " + path_save_turkic_files + "/*"
        subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)

        logging.debug("delete_temp_files: deleted successfully")
    except:
        logging.debug("delete_temp_files: delete failed")

## start

if len(sys.argv) < 2:
    print "please enter merge rate, you may also add part num"
    exit()

startScriptTime = datetime.now()

date = datetime.today() + timedelta(2)
date = str(date)
videoDatesList = findVideosDates(date)

part = 0
if len(sys.argv) < 3:
    min = 0
    max = len(videoDatesList)
else:
    part = int(sys.argv[2])
    if part == 0:
        min = 0
        max = len(videoDatesList)
    elif part == 1:
        min = 0
        max = 70    ##70
    elif part == 2:
        min = 70
        max = 100   #100
    elif part == 3:
        min = 100
        max = 120
    elif part == 4:
        min = 120
        max = 140
    elif part == 5:
        min = 140
        max = len(videoDatesList)
    else:
        print "error in part (not in range 1-5)"
        exit()

logname = "validate_all_days_merge_rate_" + sys.argv[1] + "_" + "part_" + str(part) +".log"
logging.basicConfig(filename=logname,
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=int(logging_level))

logging.info("found %s dates", str(len(videoDatesList)))
date_of_exec = str(datetime.today().strftime('%d-%m-%y'))

for i in range(min, max):   ## main loop
    startTime = datetime.now()

    logging.info("running on number: %s", str(i))
    logging.info("")
    logging.info("")
    runValidateOnVideos(str(videoDatesList[i]), sys.argv[1], part , logname,date_of_exec)

    runningTime = str(datetime.now() - startTime)
    logging.info("")
    logging.info("")
    logging.info("running on " + str(datetime.strftime(videoDatesList[i], '%Y-%m-%d')) + " completed after " + runningTime)

delete_temp_files()

runningTime = str(datetime.now() - startScriptTime)
logging.info("script ended after %s" ,runningTime)
