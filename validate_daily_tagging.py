import logging

import subprocess

from checkTextFiles import runOnAllTextsFilesInFolder
from runSHH import turkicVideos
from sqlVideosFromServer import findVideosNameByDate
import datetime
import sys
import ConfigParser

config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))
logging_level = config.get('logging', 'logging_level')
path_save_turkic_files = config.get('paths', 'path_save_turkic_files')
path_save_temp_pic = config.get('paths', 'path_save_temp_pic')


def delete_temp_folder(date, mergeRate):
    try:
        cmdLine = "sudo rm -rf " + path_save_temp_pic + "/merge_rate_" + mergeRate + "/" + date
        subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)

        cmdLine = "sudo rm -rf " + path_save_temp_pic + "/withErrorBB_merge_rate_" + mergeRate + "/" + date
        subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)

        cmdLine = "sudo rm -rf " + path_save_turkic_files + "/merge_rate_" + mergeRate + "/" + date
        subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)

        logging.debug("delete_temp_files: deleted successfully")
    except:
        logging.debug("delete_temp_files: delete failed")


def run(date, mergeRate, part, delete_temp_files, date_of_exec):
    logging.info("running validate_daily_tagging for date: %(s1)s  with merge rate of: %(s2)s ", {'s1': date, 's2': mergeRate})
    logging.info("Searching for videos names in " + str(date))
    videoNames = findVideosNameByDate(date)
    logging.info("Found " + str(len(videoNames)) + " videos")
    path_turkic_txt_files = path_save_turkic_files + "/merge_rate_" + mergeRate

    logging.info("starting to turkic videos.")
    turkicVideos(videoNames, date, path_turkic_txt_files, mergeRate)

    path_turkic_txt_files = path_turkic_txt_files + "/" + date
    logging.info("Running on texts.")
    runOnAllTextsFilesInFolder(path_turkic_txt_files + "/", date, mergeRate, part, date_of_exec)
    logging.info("File created successfully: Report_merge_rate_" + mergeRate + "_date_" + date)



    logging.info("deleting temp files")
    delete_temp_folder(mergeRate,date)

    logging.info(" ")
    sys.stdout.flush()

def validate(date_text):
    try:
        datetime.datetime.strptime(date_text, '%Y-%m-%d')
    except ValueError:
        raise ValueError("Incorrect data format, should be YYYY-MM-DD")
        exit()


delete_temp_files = True
if len(sys.argv) < 3:
    print "please enter date and merge rate"
    exit()
part = "part_0"
date_of_exec = datetime.datetime.today().strftime('%d-%m-%y')
if len(sys.argv) > 3:
    part = sys.argv[3]

validate(sys.argv[1])

if len(sys.argv) >5:
    logname = sys.argv[4]
    date_of_exec = sys.argv[5]
    delete_temp_files = False
else:
    logname = "validate_daily_tagging_merge_" + sys.argv[2] + "_" + part + "_date_" + sys.argv[1] +".log"

logging.basicConfig(filename=logname,
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=int(logging_level))


run(sys.argv[1],sys.argv[2], part, delete_temp_files, date_of_exec)
