import ConfigParser
import logging
import os
import subprocess
import time

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

from runSHH import createFolder
from sqlVideosFromServer import find_video_location

config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))
path_save_temp_pic = config.get('paths', 'path_save_temp_pic')
path_save_reports_and_images = config.get('paths', 'path_save_reports_and_images')
path_images_on_storage = config.get('paths', 'path_images_on_storage')


def addErrorBB(imageFileName,newImageName ,x1, x2, y1, y2, date, mergeRate, date_of_exec):

    imagePath = path_save_temp_pic + "/merge_rate_" + mergeRate + "/" + date + "/" + str(imageFileName) +".jpg"
    cmdLine=""
    try:
        time.sleep(2)
        img = Image.open(imagePath, 'r')
        draw = ImageDraw.Draw(img)

        if int(x1) < 100:       ##dont draw outside pic
            textX1 = int(x1) + 40
        else:
            textX1 = int(x1) - 40
        textY1 = int(y1)

        imageSave = path_save_temp_pic + "/withErrorBB_merge_rate_" + mergeRate + "/" + date + "/" + str(
            newImageName) + ".jpg"
        img.save(imageSave, "JPEG")
        time.sleep(2)

        font_size = 40
        font = ImageFont.truetype("/usr/share/fonts/truetype/freefont/FreeMono.ttf", font_size)
        draw.text((textX1, textY1), str(newImageName), font=font)
        img.save(imageSave, "JPEG")

        location = [int(x1), int(y1), int(x2), int(y2)]
        draw.rectangle(location, outline="red")
        img.save(imageSave, "JPEG")
        time.sleep(2)
        img.save(imageSave, "JPEG")

        #copy image to report folder
        cmdLine = "sudo cp " + path_save_temp_pic + "/withErrorBB_merge_rate_" + mergeRate + "/" + date + "/" + str(newImageName) + ".jpg " + path_save_reports_and_images + "/" + date_of_exec + "/merge_rate_" + mergeRate + "/" + date
        subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
        logging.debug("addErrorBB: cmdLine: %s", cmdLine)

    except:
        logging.debug("addErrorBB: saving image (%s) with BB failed ", newImageName)
        if len(cmdLine) < 5:
            logging.debug("addErrorBB: failed before exec cmdLine")
        else:
            logging.debug("addErrorBB cmd failed: %s" ,cmdLine)

        return False

    return True


def run_cmd_line(cmdLine):
    p = subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
    p.communicate(b"input data that is passed to subprocess' stdin")

def create_video_images_folder_if_dont_exist(videoName):
    logging.debug("create_video_images_folder_if_dont_exist, videoName: " + videoName)
    path = path_images_on_storage + "/" + videoName
    if not os.path.isdir(path):
        logging.debug("path doesnot exist, creating folder and images: " + path)
        createFolder(path)
        path_to_video = find_video_location(videoName)
        cmd_line = "cd /root/vatic ;  sudo turkic extract " + path_to_video + " /storage/ext_media/groundtruth/stripping/" + videoName + "/ --no-resize"

        try:
            run_cmd_line(cmd_line)
            logging.debug("create_video_images_folder_if_dont_exist Success, cmd_line: " + cmd_line)
        except:
            logging.debug("create_video_images_folder_if_dont_exist Failed, cmd_line: " + cmd_line)


def copyImageFromStorage(videoName, frameNum, errorCounter, date, x1, x2, y1, y2, mergeRate, date_of_exec):
    logging.debug("copyImageFromStorage: error counter: %s", errorCounter)
    frameFolder = str(int(int(frameNum)/100))

    dad_frame_folder = str(int(int(frameNum)/10000))
    try:
        cmdLine = "sudo cp " + path_images_on_storage + "/" + videoName + "/" + dad_frame_folder + "/" +frameFolder + "/" + frameNum + ".jpg " + path_save_temp_pic + "/merge_rate_" + mergeRate + "/" + date
        subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
        logging.debug("copyImageFromStorage: cmdLine: %s", cmdLine)
        logging.debug("image moved from " + path_images_on_storage + " to " + path_save_temp_pic)
        return addErrorBB(imageFileName=frameNum, newImageName=errorCounter, x1=x1,
                   x2=x2, y1=y1, y2=y2, date=date, mergeRate=mergeRate, date_of_exec=date_of_exec)

    except:
        logging.debug("copyImageFromStorage FAILED: cmdLine: %s", cmdLine)
        logging.debug("return False from copyImageFromStorage for image: %s", errorCounter)
        return False

    return True


