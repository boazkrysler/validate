import MySQLdb
import logging
import hashlib

import sys
from enum import Enum

def insert_row(db, row, merge_rate, hash_object):
    command = "insert into videos_with_errors_merge_" + merge_rate + " VALUES (" + row[0] + ", " + row[1] + ", " + row[
        2] + ", "
    + row[3] + ", " + row[4] + ", 0, " + str(hash_object) + " );"
    db.query(command)


def sql_cmd_if_hash_exist(db, command, row, merge_rate,hash_object):
    db.query(command)
    logging.debug("check_if_row_exist_in_DB: sql command: %s", command)

    data = db.store_result()
    data = data.fetch_row(0)
    logging.debug("check_if_row_exist_in_DB: data returned: %s", data)

    row_list = list(data)
    if len(row_list) > 0:
        logging.info("row exist in DB: %s", str(row))
        if row_list[0][5] == '1':
            logging.info("error was fixed before, adding the row")
            insert_row(db, row, merge_rate, hash_object)
        return True
    return False

def check_if_row_exist_in_DB(row, merge_rate):
    row = row.split(" ")
    string_to_hash = str(row[0:5])
    string_with_switch_tags = row[0] + row[2] + row[1] + row[4]

    hash_object = hashlib.md5(string_to_hash.encode())
    hash_object_switch_tags = hashlib.md5(string_with_switch_tags.encode())

    db = MySQLdb.connect("localhost", "root", "viisights5344", "vatic")

    command = "SELECT TOP 1 hash_id FROM videos_with_errors_merge_" + merge_rate + " WHERE hash_id = '" + hash_object + "';"
    if sql_cmd_if_hash_exist(db, command, row, merge_rate, hash_object):
        return

    command = "SELECT TOP 1 hash_id FROM videos_with_errors_merge_" + merge_rate + " WHERE hash_id = '" \
              + hash_object_switch_tags + "';"
    if sql_cmd_if_hash_exist(db, command, row, merge_rate, hash_object):
        return

    insert_row(db, row, merge_rate, hash_object)

    db.close()
    return

logging.basicConfig(filename="insert_report_to_DB",
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.DEBUG)


def run(row, merge_rate):
    check_if_row_exist_in_DB(row,merge_rate)


run(sys.argh[0], sys.argh[1])


